DESCRIPTION = "Python version of Google's common library for parsing, formatting, storing and validating international phone numbers."
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRC_URI[md5sum] = "d646c84459fbe41607dfed28f6fee246"
SRC_URI[sha256sum] = "30ca5cb631339849b9698abea2f2e0c6b11adfdddb62136c02833654d3658777"

inherit pypi setuptools

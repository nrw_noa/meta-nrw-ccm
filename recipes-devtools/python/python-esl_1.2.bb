DESCRIPTION = "Standalone FreeSWITCH ESL Python module."
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://src/esl.c;beginline=2;endline=31;md5=fa3f9a254e11c08987a2f11a0bf737b3"

SRC_URI[md5sum] = "c9dcdaed692e4a9d52cadeec4f5a9a33"
SRC_URI[sha256sum] = "eae22e9aaa85c84471e1cba64c458689091fb4a31fb936fcf4ea0f0d589206f1"

SRC_URI += "file://fix_crosscompiling.patch"

PYPI_PACKAGE = "FreeSWITCH-ESL-Python"
inherit pypi setuptools

RDEPENDS_${PN} += "freeswitch"

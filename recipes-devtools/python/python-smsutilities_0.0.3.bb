SUMMARY = "SMS encoding and decoding utilities"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI[md5sum] = "0c5081a28aa06c13af6ee38b2f846ac4"
SRC_URI[sha256sum] = "e0ad57638e24af3d401887c0cb0add9570e3a335ff4b9e53ebb72e38c5e3519b"

inherit pypi setuptools

RDEPENDS_${PN} += "python-smspdu"

PYPI_PACKAGE = "sms_utilities"

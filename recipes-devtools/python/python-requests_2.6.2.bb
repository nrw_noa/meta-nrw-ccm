DESCRIPTION = "Python HTTP for Humans."
HOMEPAGE = "http://python-requests.org"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=58c7e163c9f8ee037246da101c6afd1e"

SRC_URI[md5sum] = "0d703e5be558566e0f8c37f960d95372"
SRC_URI[sha256sum] = "0577249d4b6c4b11fd97c28037e98664bfaa0559022fee7bcef6b752a106e505"

inherit pypi setuptools

RDEPENDS_${PN} = "python-email python-json python-netserver python-zlib"

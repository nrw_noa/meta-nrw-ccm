SUMMARY = "Python-PostgreSQL Database Adapter."
LICENSE = "LGPL-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=2c9872d13fa571e7ba6de95055da1fe2"

SRC_URI[md5sum] = "842b44f8c95517ed5b792081a2370da1"
SRC_URI[sha256sum] = "6acf9abbbe757ef75dc2ecd9d91ba749547941abaffbe69ff2086a9e37d4904c"

SRC_URI += "file://remove-pg-config.patch"

# links on libpq from postgresql
DEPENDS += "postgresql"

inherit pypi setuptools distutils

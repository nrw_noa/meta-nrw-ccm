DESCRIPTION = "Testing phone networks with GSM modules"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI[md5sum] = "4e3d9e2d3aef350336f500e6919f309a"
SRC_URI[sha256sum] = "334ec1151c23dd877d7ed8c8914b425be92f975d8fbd85b14583c4ecffc94c6f"

inherit pypi setuptools
	
RDEPENDS_${PN} += " \
python-adafruit-bbio \
python-argparse \
python-flask \
python-itsdangerous \
python-jinja2 \
python-markupsafe \
python-pyserial \
python-gsmmodem \
python-pyyaml \
python-requests \
python-werkzeug \
"

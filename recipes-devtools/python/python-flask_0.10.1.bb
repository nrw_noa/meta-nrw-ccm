SUMMARY = "An abstract syntax tree for Python with inference support."
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

SRC_URI[md5sum] = "378670fe456957eb3c27ddaef60b2b24"
SRC_URI[sha256sum] = "4c83829ff83d408b5e1d4995472265411d2c414112298f2eb4b359d9e4563373"

inherit pypi setuptools

RDEPENDS_${PN} += "python-itsdangerous python-jinja2 python-werkzeug"

PYPI_PACKAGE = "Flask"

CLEANBROKEN = "1"

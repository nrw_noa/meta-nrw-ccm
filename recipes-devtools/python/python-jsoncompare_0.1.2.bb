SUMMARY = "Json comparison tool."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI[md5sum] = "ccd5effb08b696b52364ca065c39dd21"
SRC_URI[sha256sum] = "67a0eb10cd4a6a44966de114665890b55eb6afa6cbe470e7b2613299f692bd72"

inherit pypi setuptools

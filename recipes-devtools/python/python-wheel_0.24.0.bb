DESCRIPTION = "A built-package format for Python."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI[md5sum] = "3b0d66f0d127ea8befaa5d11453107fd"
SRC_URI[sha256sum] = "ef832abfedea7ed86b6eae7400128f88053a1da81a37c00613b1279544d585aa"

inherit pypi setuptools

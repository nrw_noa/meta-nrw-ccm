DESCRIPTION = "Control an attached GSM modem: send/receive SMS messages, handle calls, etc."
LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/LGPL-3.0;md5=bfccfe952269fff2b407dd11f2f3083b"

SRC_URI[md5sum] = "1708832d3655451f6e8d8bd86621a2e3"
SRC_URI[sha256sum] = "bd823005b62e88757b870edefc9c38339cef960b97be3847330d3eda2aedef89"

inherit pypi setuptools

PYPI_PACKAGE = "python-gsmmodem"

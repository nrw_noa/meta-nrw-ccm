# Instructions to setup a CCM development environment

These instructions we're tested on a **clean** Ubuntu 14.04.4 64bits VM
running on VirtualBox (version 5.0.24).

Get the iso here: http://releases.ubuntu.com/14.04/

# Steps

## Install Ubuntu

Start a 64bits Linux VM with the Ubuntu ISO and follow the steps on
the screen. I typically also install the *Guest Addition CD* to get a
nicer screen resolution.

Once Ubuntu is installed I update the distro by doing a:

~~~~
$ sudo apt-get update
$ sudo apt-get upgrade
$ reboot
~~~~

## Install dependencies

Get basic dependencies:

~~~~
$ sudo apt-get install git python-pip fabric awscli VirtualBox
$ sudo pip install ansible
~~~~

and install Vagrant directly from their website:

~~~~
$ cd ~/Downloads
$ wget https://releases.hashicorp.com/vagrant/1.9.1/vagrant_1.9.1_x86_64.deb
$ sudo dpkg -i vagrant_1.9.1_x86_64.deb
~~~~

## CCM

Clone the repo:

~~~~
$ mkdir ~/dev; cd ~/dev
$ git clone https://github.com/facebookincubator/CommunityCellularManager.git
$ cd CommunityCellularManager/cloud
~~~~

Generate keys (from CommunityCellularManager/cloud):

~~~~
$ ./generate_keys.bash
~~~~

## Switch to trusty32

This was required to get VirtualBox to run nested VMs. Probably not
needed when running on real hardware or on AWS.

From CommunityCellularManager/cloud:
~~~~
$ sed -i 's/trusty64/trusty32/g' Vagrantfile
~~~~

## Bring up the VMs

This step takes a long time (~45min. on my machine).

From CommunityCellularManager/cloud:
~~~~
$ vagrant up
~~~~

Now that all the VMs are running, log to the **web** VM:

~~~~
$ vagrant ssh web
~~~~

and issue the following commands:

~~~~
$ ~/cloud/setup_dev.bash
$ Ctrl-D # logout
~~~~

log back in again:

~~~~
$ vagrant ssh web
~~~~

and issue the following commands:

~~~~
$ workon endaga
$ cd cloud
$ python manage.py makemigrations
$ python manage.py migrate --noinput
~~~~

## Generate fake data (optional)

If you would like to populate the database with some *fake* data:

~~~~
# Generate fake data:
$ python manage.py setup_test_db
~~~~

The fake user is **testuser** and the password is **testpw**.

## Start the django server

~~~~
$ python manage.py runserver 192.168.40.10:8000
~~~~

# Test

From the *main* VM, start firefox and open:

~~~~
http://192.168.40.10:8000
~~~~

You should see the Endaga home screen.

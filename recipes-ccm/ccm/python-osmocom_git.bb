SUMMARY = "Osmocom VTTY python client"
SECTION = "devel/python"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

require ccm.inc

SRC_URI = "git://github.com/facebookincubator/CommunityCellularManager.git;branch=master"
SRCREV = "${CCMREV}"
S = "${WORKDIR}/git/osmocom-python"

RDEPENDS_${PN} = "python-jsoncompare \
openbsc \
"

inherit setuptools

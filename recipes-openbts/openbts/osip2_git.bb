DESCRIPTION = "The GNU oSIP library is an implementation of SIP - rfc3261."
LICENSE = "LGPL-2.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=e639b5c15b4bd709b52c4e7d4e2b09a4"

SRC_URI = "git://git.savannah.gnu.org/r/osip.git;branch=master;protocol=http"
SRCREV = "d9c065033715e321f0fbd97b82bb703dabde379c"
PV = "5.0.0+git${SRCPV}"
S = "${WORKDIR}/git"

inherit autotools pkgconfig

DESCRIPTION = "Subscriber Registry API and SIP Authentication Server"
LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=2d73201eaa7756524349718536abd889"

DEPENDS = "cppzmq sqlite3 osip2"

S = "${WORKDIR}/git"

NRW_NOA_MIRROR ??= "git@gitlab.com/nrw_noa"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "sipauthserve_git.bb"
PR       := "r${REPOGITFN}"

PV   = "git${SRCPV}"
PKGV = "${PKGGITV}"

DEV_BRANCH  = "${@ 'nrw/litecell15-next' if d.getVar('NRW_CCM_DEVEL', False) == "next" else 'nrw/litecell15'}"
DEV_SRCREV  = "${AUTOREV}"
DEV_SRCURI := "gitsm://${NRW_NOA_MIRROR}/subscriberRegistry.git;protocol=ssh;branch=${DEV_BRANCH}"

REL_BRANCH  = "nrw/litecell15"
REL_SRCREV  = "47f873fb2ff1aaa0573cce69dd86dd332ac5b75a"
REL_SRCURI := "gitsm://${NRW_NOA_MIRROR}/subscriberRegistry.git;protocol=ssh;branch=${REL_BRANCH}"

BRANCH  = "${@ '${DEV_BRANCH}' if d.getVar('NRW_CCM_DEVEL', False) else '${REL_BRANCH}'}"
SRCREV  = "${@ '${DEV_SRCREV}' if d.getVar('NRW_CCM_DEVEL', False) else '${REL_SRCREV}'}"
SRC_URI = "${@ '${DEV_SRCURI}' if d.getVar('NRW_CCM_DEVEL', False) else '${REL_SRCURI}'}"

addtask showversion after do_compile before do_install
do_showversion() {
    bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}

inherit autotools-brokensep pkgconfig

FILES_${PN}-dbg += "/OpenBTS/.debug /usr/local/bin/.debug"
FILES_${PN} += "/OpenBTS /usr/local/bin"

DESCRIPTION = "A neat tool for creating GDB readable coredumps from multithreaded applications"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://COPYING;md5=8a037b04a1c60affc21a7970fec529df"

SRC_URI = "https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/google-coredumper/${PN}-${PV}.tar.gz \
file://disable_unittests.patch"
SRC_URI[md5sum] = "05b88bb36c4ba41df4c7d99169b795eb"
SRC_URI[sha256sum] = "c7dc2f0ee2b00a3192bd1fa595d175ce6ecf6c5499220ba1232fa2fb4cc3774d"

inherit autotools pkgconfig
